GOI technical test
========================

Welcome to the GOI technical test. In this test we will take into account the code style, architecture and understanding of the problem.

The test will be performed in the PHP Symfony framework in version 3.4. With this test we will see how familiar you are with the MVC pattern.

Follow the instructions and if you don't understand a part of it, write an email to daniel@letsgoi.com and miguel@letsgoi.com.

Requirements
--------------

Using Clean Architecture and Symfony 3.4 and this template, create two services:

* one that allows to find for recipes using a user-provided search query
* one that returns the necessary data to feed the recipe list as shown in the following screenshot of the app [Runtasty][1]:

![alt text](https://i.blogs.es/1d1b32/runtasty-4/450_1000.jpg)

The recipe data will be obtained from the [RecipePuppy][2] API using either JSON or XML.

The crafted services should be RESTful and output JSON data. Choose the names you find appropiate and easy to use for the endpoint routes, properties, etc.

The proposed solution must be send as a GitHub, GitLab or Bitbucket repository with full git commit history.

Evaluation criteria
--------------
* The services work and return what is expected of them
* Symfony 3 Best Practices are used and respected
* PSR-1, PSR-2 and PSR-4 compliance
* Bonus points for unit test and git-flow


Data about the test
--------------
* The exercise will take about 48 hours
* You are free to make the API requests the way you like. We do use Guzzle, choose whatever you are comfortable with.

[1]:  https://play.google.com/store/apps/details?id=com.runtastic.android.runtasty.lite
[2]:  http://www.recipepuppy.com/about/api
