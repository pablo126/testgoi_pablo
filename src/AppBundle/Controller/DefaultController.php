<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations as FOSRestBundleAnnotations;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View as FOSView;
use FOS\UserBundle\Doctrine\UserManager;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultController
 *
 * @RouteResource("Default")
 * @package AppBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * Get a example. You have to rely on Nelmio to generate the REST API documentation.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @FOSRestBundleAnnotations\Get("/")
     *
     * @ApiDoc(
     *     section="Default" ,
     *     description="Get a example",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         404 = "Returned on object not found",
     *         401 = "Returned when not authenticated"
     *     },
     *     views = { "default" }
     * )
     *
     *
     * @return JsonResponse
     */
    public function getAction(Request $request)
    {
      return new JsonResponse(array("Las llamadas posibles son: 1-/receta/*texto de consulta*   2-/receta/*numero de pagina del listado(por defecto=1)*"
      ));
    }

    /**
     * Post a example. You have to rely on Nelmio to generate the REST API documentation.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @FOSRestBundleAnnotations\Post("/")
     *
     * @ApiDoc(
     *     section="Default" ,
     *     description="Post a example",
     *     statusCodes = {
     *         200 = "Returned when successful",
     *         404 = "Returned on object not found",
     *         401 = "Returned when not authenticated"
     *     },
     *     views = { "default" }
     * )
     *
     *
     * @return JsonResponse
     */
    public function postAction(Request $request)
    {
        return new JsonResponse(array(
            "prueba" => "Respuesta para la prueba de GOI"
        ));
    }
}
