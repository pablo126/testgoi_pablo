<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class RecetaController extends Controller
{
    /**
    * @ApiDoc(
    *     section="Recetas" ,
    *     description="Obtiene un listado de recetas usando como parametro el numero de página del listado",
    *     parameters={
    *       {"listado"="numero de pagina","dataType"="integer","required"=false}
    *     }
    * )
    * @return JsonResponse
    */
    public function listAction($page=1)
    {
      $client = new \GuzzleHttp\Client();
      $response = $client->request('GET', 'http://www.recipepuppy.com/api/?p='.$page);
      return new JsonResponse(json_decode($response->getBody()->getContents(), true));
    }

    /**
    * @ApiDoc(
    *     section="Recetas" ,
    *     description="Devuelve el resultado de una consulta sobre el listado de recetas",
    *     parameters={
    *       {"consulta"="cualquier palabra","dataType"="string","required"=false}
    *     }
    * )
    * @return JsonResponse
    */
    public function searchAction($query)
    {
      $client = new \GuzzleHttp\Client();
      $response = $client->request('GET', 'http://www.recipepuppy.com/api/?q='.$query);
      return new JsonResponse(json_decode($response->getBody()->getContents(), true));
    }
}
